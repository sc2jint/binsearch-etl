package com.binsearch.etl;

import com.binsearch.etl.entity.db.*;
import com.binsearch.etl.orm.JdbcUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.persistence.Basic;
import javax.persistence.Column;
import java.util.ArrayList;
import java.util.Objects;

public class Constant {

//    public static void createFileType3Feature(String language,int count) {
//        String[] temp = {
//                "CREATE TABLE `t_sourcecode_%s_filetype3feature%d` (" +
//                        "`id` bigint(12) NOT NULL AUTO_INCREMENT," +
//                        "`component_file_id` bigint(12) NOT NULL,"+
//                        "`component_id` char(100) NOT NULL,"+
//                        "`types` mediumtext," +
//                        "`type1` varchar(50) DEFAULT NULL," +
//                        "`type2` varchar(50) DEFAULT NULL," +
//                        "`type3` varchar(50) DEFAULT NULL," +
//                        "`type4` varchar(50) DEFAULT NULL," +
//                        "`type5` varchar(50) DEFAULT NULL," +
//                        "`type6` varchar(50) DEFAULT NULL," +
//                        "`type7` varchar(50) DEFAULT NULL," +
//                        "`type8` varchar(50) DEFAULT NULL," +
//                        "`type9` varchar(50) DEFAULT NULL," +
//                        "`type10` varchar(50) DEFAULT NULL," +
//                        "`type11` varchar(50) DEFAULT NULL," +
//                        "`type12` varchar(50) DEFAULT NULL," +
//                        "`type13` varchar(50) DEFAULT NULL," +
//                        "`type14` varchar(50) DEFAULT NULL," +
//                        "`type15` varchar(50) DEFAULT NULL," +
//                        "`type16` varchar(50) DEFAULT NULL," +
//                        "`type17` varchar(50) DEFAULT NULL," +
//                        "`type18` varchar(50) DEFAULT NULL," +
//                        "`type19` varchar(50) DEFAULT NULL," +
//                        "`type20` varchar(50) DEFAULT NULL," +
//                        "`type21` varchar(50) DEFAULT NULL," +
//                        "`type22` varchar(50) DEFAULT NULL," +
//                        "`type23` varchar(50) DEFAULT NULL," +
//                        "`type24` varchar(50) DEFAULT NULL," +
//                        "`type25` varchar(50) DEFAULT NULL," +
//                        "`type26` varchar(50) DEFAULT NULL," +
//                        "`type27` varchar(50) DEFAULT NULL," +
//                        "`type28` varchar(50) DEFAULT NULL," +
//                        "`type29` varchar(50) DEFAULT NULL," +
//                        "`type30` varchar(50) DEFAULT NULL," +
//                        "`type31` varchar(50) DEFAULT NULL," +
//                        "`type32` varchar(50) DEFAULT NULL," +
//                        "`type33` varchar(50) DEFAULT NULL," +
//                        "`type34` varchar(50) DEFAULT NULL," +
//                        "`type35` varchar(50) DEFAULT NULL," +
//                        "`type36` varchar(50) DEFAULT NULL," +
//                        "`type37` varchar(50) DEFAULT NULL," +
//                        "`type38` varchar(50) DEFAULT NULL," +
//                        "`type39` varchar(50) DEFAULT NULL," +
//                        "`type40` varchar(50) DEFAULT NULL," +
//                        "`type41` varchar(50) DEFAULT NULL," +
//                        "`type42` varchar(50) DEFAULT NULL," +
//                        "`type43` varchar(50) DEFAULT NULL," +
//                        "`type44` varchar(50) DEFAULT NULL," +
//                        "`type45` varchar(50) DEFAULT NULL," +
//                        "`type46` varchar(50) DEFAULT NULL," +
//                        "`type47` varchar(50) DEFAULT NULL," +
//                        "`type48` varchar(50) DEFAULT NULL," +
//                        "`type49` varchar(50) DEFAULT NULL," +
//                        "`type50` varchar(50) DEFAULT NULL," +
//                        "`type51` varchar(50) DEFAULT NULL," +
//                        "`type52` varchar(50) DEFAULT NULL," +
//                        "`type53` varchar(50) DEFAULT NULL," +
//                        "`type54` varchar(50) DEFAULT NULL," +
//                        "`type55` varchar(50) DEFAULT NULL," +
//                        "`type56` varchar(50) DEFAULT NULL," +
//                        "`type57` varchar(50) DEFAULT NULL," +
//                        "`type58` varchar(50) DEFAULT NULL," +
//                        "`type59` varchar(50) DEFAULT NULL," +
//                        "`type60` varchar(50) DEFAULT NULL," +
//                        "`source_table` varchar(255) DEFAULT NULL," +
//                        "`create_date` timestamp," +
//                        "PRIMARY KEY (`id`)," +
//                        "KEY `t_file_feature_component_id` (`component_id`),",
//                "KEY `t_file_feature_component_file_id` (`component_file_id`),",
//                "KEY `t_file_feature_component_create_date` (`create_date`),",
//                "KEY `t_file_feature_component_create_type1` (`type1`),",
//                "KEY `t_file_feature_component_create_type2` (`type2`),",
//                "KEY `t_file_feature_component_create_type3` (`type3`),",
//                "KEY `t_file_feature_component_create_type4` (`type4`),",
//                "KEY `t_file_feature_component_create_type5` (`type5`),",
//                "KEY `t_file_feature_component_create_type6` (`type6`),",
//                "KEY `t_file_feature_component_create_type7` (`type7`),",
//                "KEY `t_file_feature_component_create_type8` (`type8`),",
//                "KEY `t_file_feature_component_create_type9` (`type9`),",
//                "KEY `t_file_feature_component_create_type10` (`type10`),",
//                "KEY `t_file_feature_component_create_type11` (`type11`),",
//                "KEY `t_file_feature_component_create_type12` (`type12`),",
//                "KEY `t_file_feature_component_create_type13` (`type13`),",
//                "KEY `t_file_feature_component_create_type14` (`type14`),",
//                "KEY `t_file_feature_component_create_type15` (`type15`),",
//                "KEY `t_file_feature_component_create_type16` (`type16`),",
//                "KEY `t_file_feature_component_create_type17` (`type17`),",
//                "KEY `t_file_feature_component_create_type18` (`type18`),",
//                "KEY `t_file_feature_component_create_type19` (`type19`),",
//                "KEY `t_file_feature_component_create_type20` (`type20`),",
//                "KEY `t_file_feature_component_create_type21` (`type21`),",
//                "KEY `t_file_feature_component_create_type22` (`type22`),",
//                "KEY `t_file_feature_component_create_type23` (`type23`),",
//                "KEY `t_file_feature_component_create_type24` (`type24`),",
//                "KEY `t_file_feature_component_create_type25` (`type25`),",
//                "KEY `t_file_feature_component_create_type26` (`type26`),",
//                "KEY `t_file_feature_component_create_type27` (`type27`),",
//                "KEY `t_file_feature_component_create_type28` (`type28`),",
//                "KEY `t_file_feature_component_create_type29` (`type29`),",
//                "KEY `t_file_feature_component_create_type30` (`type30`),",
//                "KEY `t_file_feature_component_create_type31` (`type31`),",
//                "KEY `t_file_feature_component_create_type32` (`type32`),",
//                "KEY `t_file_feature_component_create_type33` (`type33`),",
//                "KEY `t_file_feature_component_create_type34` (`type34`),",
//                "KEY `t_file_feature_component_create_type35` (`type35`),",
//                "KEY `t_file_feature_component_create_type36` (`type36`),",
//                "KEY `t_file_feature_component_create_type37` (`type37`),",
//                "KEY `t_file_feature_component_create_type38` (`type38`),",
//                "KEY `t_file_feature_component_create_type39` (`type39`),",
//                "KEY `t_file_feature_component_create_type40` (`type40`),",
//                "KEY `t_file_feature_component_create_type41` (`type41`),",
//                "KEY `t_file_feature_component_create_type42` (`type42`),",
//                "KEY `t_file_feature_component_create_type43` (`type43`),",
//                "KEY `t_file_feature_component_create_type44` (`type44`),",
//                "KEY `t_file_feature_component_create_type45` (`type45`),",
//                "KEY `t_file_feature_component_create_type46` (`type46`),",
//                "KEY `t_file_feature_component_create_type47` (`type47`),",
//                "KEY `t_file_feature_component_create_type48` (`type48`),",
//                "KEY `t_file_feature_component_create_type49` (`type49`),",
//                "KEY `t_file_feature_component_create_type50` (`type50`),",
//                "KEY `t_file_feature_component_create_type51` (`type51`),",
//                "KEY `t_file_feature_component_create_type52` (`type52`),",
//                "KEY `t_file_feature_component_create_type53` (`type53`),",
//                "KEY `t_file_feature_component_create_type54` (`type54`),",
//                "KEY `t_file_feature_component_create_type55` (`type55`),",
//                "KEY `t_file_feature_component_create_type56` (`type56`),",
//                "KEY `t_file_feature_component_create_type57` (`type57`),",
//                "KEY `t_file_feature_component_create_type58` (`type58`),",
//                "KEY `t_file_feature_component_create_type59` (`type59`),",
//                "KEY `t_file_feature_component_create_type60` (`type60`),",
//                "KEY `t_file_feature_component_id_file_id` (`component_id`,`component_file_id`)",
//                ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb3;"
//        };
//
//        for(int i=1; i<=count; i++) {
//            Exception exception = new JdbcUtils(jdbcTemplate)
//                    .model(FileType3Feature.class)
//                    .table(String.format("t_sourcecode_%s_filetype3feature%d", language, i))
//                    .limit(1)
//                    .query(new ArrayList<FileFeature>())
//                    .error();
//
//            if(Objects.nonNull(exception)) {
//                jdbcTemplate.update(String.format(StringUtils.join(temp), language, i));
//            }
//        }
//    }


    public static void creatComponentVersionInfo(String tableName, JdbcTemplate jdbcTemplate){
        String[] temp = {
                "CREATE TABLE `%s` (",
                "`id` bigint(12) NOT NULL AUTO_INCREMENT,",
                "`component_id` char(100) DEFAULT NULL,",
                "`component_version` char(100),",
                "`component_info` text DEFAULT NULL,",
                "`archive_name` char(100),",
                "`file_size` bigint(16),",
                "`file_size_encode` char(20),",
                "PRIMARY KEY (`id`),",
                "KEY `t_component_id` (`component_id`),",
                "KEY `t_component_version_` (`component_id`,`component_version`)",
                ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb3;",
        };


        Exception exception = new JdbcUtils(jdbcTemplate)
                .model(ComponentVersionInfo.class)
                .table(tableName)
                .limit(1)
                .query(new ArrayList<ComponentVersionInfo>())
                .error();

        if(Objects.nonNull(exception)){
            jdbcTemplate.update(String.format(StringUtils.join(temp),tableName));
        }
    }

    public static void creatComponentFile(String tableName, JdbcTemplate jdbcTemplate){
        String[] temp = {
                "CREATE TABLE `%s` (",
                    "`id` bigint(12) NOT NULL AUTO_INCREMENT,",
                    "`component_id` char(100) DEFAULT NULL,",
                    "`source_file_path` mediumtext,",
                    "`file_path` char(150) DEFAULT NULL,",
                    "`language` char(50) DEFAULT NULL,",
                    "`component_version_id` mediumtext,",
                    "`file_hash_value` char(255) DEFAULT NULL,",
                    "`file_size` bigint(16) DEFAULT NULL,",
                    "PRIMARY KEY (`id`),",
                    "KEY `t_file_component_id` (`component_id`),",
                    "KEY `t_file_hash_value` (`file_hash_value`),",
                    "KEY `t_component_file_hash_value` (`component_id`,`file_hash_value`),",
                    "KEY `t_language` (`language`)",
                ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb3;",
        };


        Exception exception = new JdbcUtils(jdbcTemplate)
                .model(ComponentFile.class)
                .table(tableName)
                .limit(1)
                .query(new ArrayList<ComponentFile>())
                .error();

        if(Objects.nonNull(exception)){
            jdbcTemplate.update(String.format(StringUtils.join(temp),tableName));

        }
    }

    public static void createFileFeature(String tableName, JdbcTemplate jdbcTemplate) {
        String[] temp = {
                "CREATE TABLE `%s` (" +
                        "`id` bigint(12) NOT NULL AUTO_INCREMENT," +
                        "`component_file_id` bigint(12) NOT NULL,"+
                        "`component_id` char(100) NOT NULL," +
                        "`line_number` int DEFAULT NULL," +
                        "`token_number` int DEFAULT NULL," +
                        "`type0` varchar(255) DEFAULT NULL," +
                        "`type1` varchar(255) DEFAULT NULL," +
                        "`type2blind` varchar(255) DEFAULT NULL," +
                        "`source_table` varchar(255) DEFAULT NULL," +
                        "`create_date` timestamp," +
                        "PRIMARY KEY (`id`)," +
                        "KEY `t_file_feature_component_id` (`component_id`),",
                "KEY `t_file_feature_component_file_id` (`component_file_id`),",
                "KEY `t_file_feature_component_create_date` (`create_date`),",
                "KEY `t_file_feature_component_type0` (`type0`),",
                "KEY `t_file_feature_component_type1` (`type1`),",
                "KEY `t_file_feature_type2blind` (`type2blind`),",
                "KEY `t_file_feature_component_source_table` (`source_table`),",
                "KEY `t_file_feature_component_id_file_id` (`component_id`,`component_file_id`)",
                ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb3;"
        };


        Exception exception = new JdbcUtils(jdbcTemplate)
                    .model(FileFeature.class)
                    .table(tableName)
                    .limit(1)
                    .query(new ArrayList<FileFeature>())
                    .error();

        if(Objects.nonNull(exception)) {
            jdbcTemplate.update(String.format(StringUtils.join(temp), tableName));
        }

    }

    public static void createFuncFeature(String tableName,JdbcTemplate jdbcTemplate) {
        String[] temp = {
                "CREATE TABLE `%s` (" +
                        "`id` bigint(12) NOT NULL AUTO_INCREMENT," +
                        "`component_file_id` bigint(12) NOT NULL,"+
                        "`component_id` char(100) NOT NULL," +
                        "`start_line` int DEFAULT NULL," +
                        "`end_line` int DEFAULT NULL," +
                        "`line_number` int DEFAULT NULL," +
                        "`token_number` int DEFAULT NULL," +
                        "`type0` varchar(255) DEFAULT NULL," +
                        "`type1` varchar(255) DEFAULT NULL," +
                        "`type2blind` varchar(255) DEFAULT NULL," +
                        "`source_table` varchar(255) DEFAULT NULL," +
                        "`create_date` timestamp," +
                        "PRIMARY KEY (`id`)," +
                        "KEY `t_func_feature_component_id` (`component_id`),",
                "KEY `t_func_feature_component_create_date` (`create_date`),",
                "KEY `t_func_feature_component_file_id` (`component_file_id`),",
                "KEY `t_func_feature_type0` (`type0`),",
                "KEY `t_func_feature_type1` (`type1`),",
                "KEY `t_func_feature_component_id_file_id` (`component_id`,`component_file_id`)",
                ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb3;"
        };

        Exception exception = new JdbcUtils(jdbcTemplate)
                .model(FuncFeature.class)
                .table(tableName)
                .limit(1)
                .query(new ArrayList<FuncFeature>())
                .error();

        if(Objects.nonNull(exception)) {
            jdbcTemplate.update(String.format(StringUtils.join(temp), tableName));
        }
    }

    public enum SourceCodeType {
        GIT(".zip"),

        MAVEN(".jar");


        private String archive;

        SourceCodeType(String archive){
            this.archive = archive;
        }

        public String getArchive(){
            return this.archive;
        }
    }

    public enum LanguageType {
        JAVA("JAVA","java",new String[]{"java"}),
        C("C","c",new String[]{"c"}),
        CPP("C++","cpp",new String[]{"cpp","hpp"}),
        PYTHON("PYTHON","python",new String[]{"py"}),
        GOLANG("GOLANG","golang",new String[]{"go"}),
        JAVASCRIPT("JAVASCRIPT","javascript",new String[]{"js"}),
        PHP("PHP","php",new String[]{"php"}),
        RUBY("RUBY","ruby",new String[]{"rb"}),
        RUST("RUST","rust",new String[]{"rs"}),
        CS("C#","cs",new String[]{"cs"});

        //开发语言
        private String languageName;

        //别名
        private String alias;

        //文件后缀名
        private String[] expandNames;

        LanguageType(String languageName,String alias,String[] expandNames){
            this.expandNames = expandNames;
            this.alias = alias;
            this.languageName = languageName;
        }

        public String getAlias(){
            return this.alias;
        }

        public String getLanguageName(){
            return this.languageName;
        }

        public String[] getExpandNames(){
            return this.expandNames;
        }

        public static LanguageType getLanguageTypeForAlias(String alias){
            LanguageType temp = null;
            for(LanguageType model: LanguageType.values()){
                if(model.getAlias().equals(alias)){
                    temp = model;
                    break;
                }
            }
            return temp;
        }

        public static LanguageType getLanguageTypeForLanguageName(String languageName){
            LanguageType temp = null;
            for(LanguageType model: LanguageType.values()){
                if(model.getLanguageName().equals(languageName)){
                    temp = model;
                    break;
                }
            }
            return temp;
        }

    }

    public enum ModelType{

        MODEL_COMPONENT_INFO(
                "componentVersionInfo",
                "t_component_version_info",
                "componentVersionInfoName",
                "component_version_info_name",
                "_version_info",
                ComponentVersionInfo.class,
                4),


        MODEL_COMPONENT_FILE(
                "componentFile",
                "t_component_file",
                "componentFileName",
                "component_file_name",
                "_component_file",
                ComponentFile.class,
                40),

        MODEL_FILE_TYPE3_FEATURE(
                "fileType3Feature",
                "t_sourcecode_%s_filetype3feature",
                "sourcecode%sFiletype3featureName",
                "sourcecode_%s_filetype3feature_name",
                "_filetype3feature_",
                 FileType3Feature.class,
                10
        ),

        MODEL_FILE_FEATURE(
                "fileFeature",
                "t_sourcecode_%s_filefeature",
                "sourcecode%sFilefeatureName",
                "sourcecode_%s_filefeature_name",
                "_filefeature",
                FileFeature.class,
                10),

        MODEL_FUNC_FEATURE(
                "funcFeature",
                "t_sourcecode_%s_funcfeature",
                "sourcecode%sFuncfeatureName",
                "sourcecode_%s_funcfeature_name",
                "_funcfeature",
                FuncFeature.class,
                70);

        //数据表名特征
        private String dataBaseTableNameFeature;

        //类型名
        private String modelName;

        //对应的表名模式
        private String tableNameModel;

        //对应缓存表属性名
        private String componentCacheAttrName;

        //对于缓存表列名
        private String componentCacheTableColumnName;

        //业务实体类
        private Class entityClass;

        //数据表数量
        private Integer tableCount;

        public String getDataBaseTableNameFeature(){
            return this.dataBaseTableNameFeature;
        }


        public Class getEntityClass() {
            return this.entityClass;
        }

        public String getModelName(){
            return this.modelName;
        }

        public String getTableNameModel(){
            return this.tableNameModel;
        }

        public String getComponentCacheAttrName(){
            return this.componentCacheAttrName;
        }

        public String getComponentCacheTableColumnName(){
            return this.componentCacheTableColumnName;
        }

        public Integer getTableCount(){return this.tableCount;};

        ModelType(String modelName,String tableNameModel,
                  String componentCacheAttrName,String componentCacheTableColumnName,
                  String dataBaseTableNameFeature,Class entityClass,Integer tableCount){
            this.modelName = modelName;
            this.tableNameModel = tableNameModel;
            this.componentCacheAttrName = componentCacheAttrName;
            this.componentCacheTableColumnName = componentCacheTableColumnName;
            this.dataBaseTableNameFeature = dataBaseTableNameFeature;
            this.entityClass = entityClass;
            this.tableCount = tableCount;
        }

        public static ModelType getModelType(String modelName){
            ModelType temp = null;
            for(ModelType model: ModelType.values()){
                if(model.getModelName().equals(modelName)){
                    temp = model;
                    break;
                }
            }
            return temp;
        }

    }
}
