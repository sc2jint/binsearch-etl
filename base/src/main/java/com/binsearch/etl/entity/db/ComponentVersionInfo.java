package com.binsearch.etl.entity.db;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_component_version_info")
public class ComponentVersionInfo {

      @Id
      @Column(name = "id")
      public Long id;

      @Basic
      @Column(name = "component_id")
      public String componentId;


      @Basic
      @Column(name = "component_version")
      public String componentVersion;

      @Basic
      @Column(name = "archive_name")
      public String archiveName;

      @Basic
      @Column(name = "component_info")
      public String componentInfo;

      @Basic
      @Column(name="file_size")
      public Long fileSize;

      @Basic
      @Column(name="file_size_encode")
      public String fileSizeEncode;


}
