package com.binsearch.front;

public interface CommandInfc {

    //命令执行
    public boolean execCommand(String command,CommandInfc obj);

    //命令描述
    public void descCommand();

    //命令资源释放
    public void closeCommand();
}
