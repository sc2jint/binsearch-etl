package com.binsearch.front;

public interface CommandMessage {

    public enum CommandRunningFlag{
        INIT,
        RUNNING,
        END;
    }
}
