package com.binsearch.front;

import com.binsearch.front.inout.InOutUtils;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.util.Strings;

import java.io.File;
import java.util.Objects;
import java.util.Scanner;
import java.util.function.Function;

public class ConfigFileManage {

    public static <T,R> R loadConfigFile(String configFileName, Function<String,R> function){
        File dir = new File("");
        dir =  new File(dir.getAbsolutePath());

        File[] files = dir.listFiles();



        File configFile = null;
        for (File file : files) {
            if (file.getName().equals(configFileName)) {
                do {
                    System.out.print(String.format("==== 搜索到配置文件,%s,是否加载(y/n):", file.getAbsolutePath()));

                    Scanner in = new Scanner(System.in);
                    String temp = in.nextLine();

                    if (InOutUtils.COMMAND_ATTR_YES.equalsIgnoreCase(temp)) {
                        configFile = file;
                        break;
                    } else if (InOutUtils.COMMAND_ATTR_NO.equalsIgnoreCase(temp)) {
                        break;
                    } else {
                        System.out.println("==== 非指定参数请重新确认");
                    }
                } while (true);
            }
        }

        if (Objects.isNull(configFile)) {
            do {
                System.out.print("==== 请输入配置文件路径:");

                Scanner in = new Scanner(System.in);
                String temp = in.nextLine();

                if (Strings.isNotBlank(temp)) {
                    configFile = new File(temp);

                    if (!configFile.exists() || !configFile.isFile()) {
                        System.out.println("======== 指定文件不正确");
                        continue;
                    }
                    break;
                }

                System.out.print("==== 非指定参数请重新确认");
            } while (true);
        }

        String json = null;
        try {
            json = FileUtils.readFileToString(configFile);
        } catch (Exception e) {
            System.out.print("==== 读取配置文件失败:" + e.getMessage());
        }

        if (Strings.isNotBlank(json)) {
            R r = function.apply(json);
            System.out.println("==== 加载配置文件成功");
            return r;
        }
        System.out.println("==== 加载配置文件失败");


        return null;
    }
}
