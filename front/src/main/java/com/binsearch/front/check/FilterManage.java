package com.binsearch.front.check;

import com.binsearch.etl.Constant;
import com.binsearch.front.FrontBaseEngineConfiguration;
import com.binsearch.front.inout.InOutUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.util.Strings;

import java.io.File;
import java.nio.charset.Charset;
import java.util.*;
import java.util.function.Function;

public class FilterManage {

   public enum AttrType {
        COMMAND_ATTR_LANGUAGE_TYPES("languageTypes","指定组件语言类型",Arrays.asList(Constant.LanguageType.values())),

        COMMAND_ATTR_EXEC_COMPONENT_ID ("componentIds","指定组件id",new ArrayList());

        private String key;

        private String desc;

        private List defValue;

        AttrType(String key,String desc,List defValue){
            this.key = key;
            this.defValue = defValue;
            this.desc = desc;
        }

        public String getKey(){
            return this.key;
        }

        public String getDesc(){
            return this.desc;
        }

        public List getDefValue(){
            return this.defValue;
        }
    }

    public static boolean getCheckDataFilterObj(CheckDataFilterObj filter) throws Exception{


        if (filter.getAttrFlag() == FrontBaseEngineConfiguration.AttrFlag.SETING) {
            System.out.println("==== 过滤配置已存在");

            if (Objects.nonNull(filter.getComponentIds())) {
                StringBuffer temp = new StringBuffer();
                filter.getComponentIds().forEach(i -> {
                    temp.append(i).append(",");
                });

                System.out.println(String.format("==== %s,%s:%s", AttrType.COMMAND_ATTR_EXEC_COMPONENT_ID.getDesc(),
                        AttrType.COMMAND_ATTR_EXEC_COMPONENT_ID.getKey(), temp.toString()));
            }

            do {
                try {
                    System.out.println();
                    System.out.print("==== 是否重新配置?(y/n):");

                    Scanner in = new Scanner(System.in);
                    String temp = in.nextLine();

                    if (InOutUtils.COMMAND_ATTR_YES.equalsIgnoreCase(temp)) {
                        filter.clear();
                        break;
                    } else if (InOutUtils.COMMAND_ATTR_NO.equalsIgnoreCase(temp)) {
                        break;
                    }

                    System.out.println("==== 非指定参数请重新确认");
                } catch (Exception e) {
                }
            } while (true);
        }


        if (filter.getAttrFlag() == FrontBaseEngineConfiguration.AttrFlag.UNSETING ||
                filter.getAttrFlag() == FrontBaseEngineConfiguration.AttrFlag.INIT) {
            Map<AttrType, Function<AttrType, List>> linkedHashMap = new LinkedHashMap<AttrType, Function<AttrType, List>>();

            linkedHashMap.put(AttrType.COMMAND_ATTR_EXEC_COMPONENT_ID, new Function<AttrType, List>() {
                @Override
                public List apply(AttrType attrType) {
                    List<String> l = new ArrayList<String>();

                    do {
                        System.out.println();
                        System.out.print("==== 是否导入全部组件?(y/n):");

                        Scanner in = new Scanner(System.in);
                        String temp = in.nextLine();

                        if (InOutUtils.COMMAND_ATTR_NO.equalsIgnoreCase(temp)) {
                            loadComponentFile(l);
                            break;
                        } else if (InOutUtils.COMMAND_ATTR_YES.equalsIgnoreCase(temp)) {
                            break;
                        } else {
                            System.out.println("==== 非指定参数请重新确认");
                        }
                    } while (true);

                    return l;
                }
            });


            Map<AttrType, List> values = new LinkedHashMap<AttrType, List>();

            linkedHashMap.forEach((k, v) -> {
                values.put(k, v.apply(k));
            });

            filter.setComponentIds(values.get(AttrType.COMMAND_ATTR_EXEC_COMPONENT_ID));

            filter.setAttrFlag(FrontBaseEngineConfiguration.AttrFlag.SETING);
            System.out.println("==== 过滤条件设置成功!");
        }
        return true;
    }


    public static void loadComponentFile(List<String> commandIds){
        do {
            System.out.print("==== 是否引入组件文件?(y/n):");

            Scanner in = new Scanner(System.in);
            String temp = in.nextLine();

            if (InOutUtils.COMMAND_ATTR_YES.equalsIgnoreCase(temp)) {
                do {
                    System.out.print("======== 输入组件文件路径(组件以'@'分割):");

                    Scanner in1 = new Scanner(System.in);
                    String str = in1.nextLine();

                    //加载组件文件
                    File file = new File(str);
                    if (file.exists() && file.isFile()) {
                        try {
                            str = FileUtils.readFileToString(file, Charset.defaultCharset());

                            String[] commands = str.split("@");
                            commandIds.addAll(Arrays.asList(commands));

                            if (commandIds.isEmpty()) {
                                System.out.println("======== 最少存在一个组件");
                                continue;
                            }
                            break;
                        } catch (Exception e) {
                            System.out.println("======== 读取组件文件出错," + e.getMessage());
                        }
                    } else {
                        System.out.println("======== 指定文件不存在");
                    }
                } while (true);
                break;
            } else if (InOutUtils.COMMAND_ATTR_NO.equalsIgnoreCase(temp)) {
                System.out.println("======== 输入 q 结束组件指定");

                int num = 1;
                do {
                    System.out.print(String.format("======== [%d]:", num));
                    Scanner in1 = new Scanner(System.in);
                    String str = in1.nextLine();

                    if ("q".equalsIgnoreCase(str)) {
                        if (commandIds.isEmpty()) {
                            System.out.println("======== 最少存在一个组件");
                        }
                        break;
                    } else if (Strings.isNotBlank(str)) {
                        commandIds.add(str);
                        num++;
                    } else {
                        System.out.println("======== 非指定参数请重新确认");
                    }
                } while (true);
                break;
            } else {
                System.out.println("======== 非指定参数请重新确认");
            }
        } while (true);
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CheckDataFilterObj{

        //组件语言过滤
        List<Constant.LanguageType> languageTypes;

        //指定组件
        List<String> componentIds;

        //是否处理数据
        FrontBaseEngineConfiguration.ExecDataType execDataType;

        FrontBaseEngineConfiguration.AttrFlag attrFlag = FrontBaseEngineConfiguration.AttrFlag.INIT;

        public void clear(){
            this.languageTypes = new ArrayList<Constant.LanguageType>();
            this.componentIds = new ArrayList<>();
            this.execDataType = FrontBaseEngineConfiguration.ExecDataType.SQL;
            this.attrFlag = FrontBaseEngineConfiguration.AttrFlag.UNSETING;
        }
    }




}
