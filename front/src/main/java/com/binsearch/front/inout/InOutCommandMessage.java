package com.binsearch.front.inout;

import com.binsearch.front.CommandMessage;
import com.binsearch.front.FrontBaseEngineConfiguration;
import com.binsearch.front.WindowsProgressBar;
import lombok.Data;

import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

@Data
public class InOutCommandMessage implements CommandMessage {

    private static final Object lock = new Object();
    private CommandMessage.CommandRunningFlag commandRunningFlag;
    private FrontBaseEngineConfiguration.ExecDataType execDataType;
    private WindowsProgressBar progressBar;
    private static InOutCommandMessage inOutCommandMessage;

    private Thread running;

    public static InOutCommandMessage getInOutCommandMessageObj(){
        if(Objects.isNull(InOutCommandMessage.inOutCommandMessage)){
            synchronized (lock){
                if(Objects.isNull(InOutCommandMessage.inOutCommandMessage)){
                    inOutCommandMessage = new InOutCommandMessage();
                }
            }
        }
        return inOutCommandMessage;
    }

    public void close(){
        synchronized (lock) {
            try {
                TimeUnit.SECONDS.sleep(2L);

                this.setCommandRunningFlag(CommandRunningFlag.END);
                this.running.join();
                inOutCommandMessage = null;
            }catch (Exception e){}
        }
    }

    public void init(FrontBaseEngineConfiguration.ExecDataType execDataType){
        this.execDataType = execDataType;
        this.commandRunningFlag = CommandRunningFlag.INIT;
        this.progressBar = new WindowsProgressBar();
    }



    //导入导出组件总数
    private AtomicInteger componentCount = new AtomicInteger(0);

    //导入导出组件完成数
    private AtomicInteger componentOkNum = new AtomicInteger(0);

    //导入导出组件完成数
    private AtomicInteger componentErrorNum = new AtomicInteger(0);

    private AtomicInteger componentNum = new AtomicInteger(0);

    private AtomicInteger componentSqlNum = new AtomicInteger(0);


    //导入导出sql保存路径
    private String sourceFilePath;

    private String targetFilePath;


    public void addComponentOkNum(int i){

        this.componentOkNum.addAndGet(i);
    }

    public void addComponentErrorNum(int i){
        this.componentErrorNum.addAndGet(i);
    }

    public void addComponentCount(int i){
        this.componentCount.addAndGet(i);
        this.progressBar.setFinishPoint(i);
    }


    public void addComponentSqlNum(int i){
        this.componentSqlNum.addAndGet(i);
    }

    public void addComponentOutNum(WorkJob job){
        this.componentNum.addAndGet(1);
        if(job.getIsOutFlag() == InOutUtils.InOutFlag.END) {
            this.addComponentOkNum(1);
            return;
        }
        this.addComponentErrorNum(1);
    }

    public void addComponentInNum(WorkJob job){
        this.componentNum.addAndGet(1);
        if(job.getIsInFlag() == InOutUtils.InOutFlag.END) {
            this.addComponentOkNum(1);
            return;
        }
        this.addComponentErrorNum(1);

    }


    Function<CommandMessage, Boolean> sqlFunc = new Function<CommandMessage, Boolean>() {
        @Override
        public Boolean apply(CommandMessage commandMessage) {
            InOutCommandMessage msg = (InOutCommandMessage)commandMessage;
            System.out.println();

            System.out.println(String.format("进度组件信息:%d/%d",(msg.getComponentOkNum().get()+msg.getComponentErrorNum().get()),msg.getComponentCount().get()));
            System.out.println(String.format("成功组件信息:%d",msg.getComponentOkNum().get()));
            System.out.println(String.format("失败组件信息:%d",msg.getComponentErrorNum().get()));
            System.out.println(String.format("执行SQL数:%d",msg.getComponentSqlNum().get()));
            System.out.println(String.format("SQL保存路径:%s",msg.getSourceFilePath()));
            return true;
        }
    };


    Function<CommandMessage, Boolean> fileFunc = new Function<CommandMessage, Boolean>() {
        @Override
        public Boolean apply(CommandMessage commandMessage) {
            InOutCommandMessage msg = (InOutCommandMessage)commandMessage;
            System.out.println();
            System.out.println(String.format("进度组件信息:%d/%d",(msg.getComponentOkNum().get()+msg.getComponentErrorNum().get()),msg.getComponentCount().get()));
            System.out.println(String.format("成功组件信息:%d",msg.getComponentOkNum().get()));
            System.out.println(String.format("失败组件信息:%d",msg.getComponentErrorNum().get()));
            System.out.println(String.format("源文件保存路径:%s",msg.getSourceFilePath()));
            System.out.println(String.format("目标文件保存路径:%s",msg.getSourceFilePath()));
            return true;
        }
    };


    public void running(){

        inOutCommandMessage.setCommandRunningFlag(CommandRunningFlag.RUNNING);



        this.running = new Thread(new Runnable() {
            @Override
            public void run() {
                do{
                    InOutCommandMessage commandMessage = getInOutCommandMessageObj();
                    if(commandMessage.getCommandRunningFlag() == CommandRunningFlag.END){
                        break;
                    }
                    Function<CommandMessage, Boolean> func = commandMessage.getSqlFunc();
                    if(commandMessage.getExecDataType() == FrontBaseEngineConfiguration.ExecDataType.FILE){
                        func = commandMessage.getFileFunc();
                    }

                    try {
                        int num = commandMessage.getComponentOkNum().get() + commandMessage.getComponentErrorNum().get();

                        commandMessage.getProgressBar()
                                .showBarByPoint(Double.valueOf(num), func,commandMessage);

                    } catch (InterruptedException e) {}
                }while (true);
            }
        });
        this.running.start();
    }



}
