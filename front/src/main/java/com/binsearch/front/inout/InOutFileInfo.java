package com.binsearch.front.inout;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class InOutFileInfo {

    List<String> filePaths = new ArrayList<String>();

    Integer countNum;

    @JSONField(serialize=false)
    List<File> files = new ArrayList<File>();


}
