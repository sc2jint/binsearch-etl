package com.binsearch.engine.component2file;


import com.binsearch.etl.entity.db.MavenDetailTasks;
import com.binsearch.etl.entity.db.ViewDetailTasks;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class WorkJob {

   private Task task;

   private Object detailTask;

   private String sourceType;

   private String sourceFilePath;


   //统计文件类型
   private Map<String,AtomicInteger> fileTypeCount = new HashMap<String,AtomicInteger>();

   //文件大小
   private Long fileSize;

   //文件大小明文
   private String fileSizeStr;

   public WorkJob(Task task,Object detailTask,String sourceType,String sourceFilePath){
      this.task = task;
      this.detailTask = detailTask;
      this.sourceType = sourceType;
      this.sourceFilePath = sourceFilePath;
   }

   //文件后缀名统计
   public void fileCount(String fileName){
      int index = fileName.lastIndexOf(".");
      if(index == -1){
         if(fileTypeCount.containsKey("other")){
            AtomicInteger num = fileTypeCount.get("other");
            num.incrementAndGet();
         }else{
            fileTypeCount.put("other",new AtomicInteger(1));
         }
         return;
      }

      String key = fileName.substring(index,fileName.length());
      if(fileTypeCount.containsKey(key)){
         AtomicInteger num = fileTypeCount.get(key);
         num.incrementAndGet();
      }else{
         fileTypeCount.put(key,new AtomicInteger(1));
      }
   }

   //获取组件ID
   public String getTaskId(){
      return sourceType.equals(Engine.SOURCE_TYPE_GIT)?
              ((ViewDetailTasks)detailTask).taskId:((MavenDetailTasks)detailTask).compoId;

   }


   //获取组件版本
   public String getVersionName(){
      return sourceType.equals(Engine.SOURCE_TYPE_GIT)?
              ((ViewDetailTasks)detailTask).versionName:((MavenDetailTasks)detailTask).currentVersion;
   }

   public void errorLog(String str){
      try {
         task.errorLog(getTaskId(), str);
      }catch (Exception e){}
   }

}
