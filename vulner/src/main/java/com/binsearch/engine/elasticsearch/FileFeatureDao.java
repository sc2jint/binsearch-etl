package com.binsearch.engine.elasticsearch;

import com.binsearch.etl.entity.el.FileFeatureES;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@ConditionalOnExpression("'${base.runningModel.service.engineRegister.ELASTICSEARCH_ENGINE}'.equalsIgnoreCase('true')")
@Repository
public interface FileFeatureDao extends ElasticsearchRepository<FileFeatureES,String>
{
    Optional<FileFeatureES> findById(String id);

    List<FileFeatureES> findByComponentId(String componentId);
}
