package com.binsearch.engine.output;

import com.binsearch.etl.entity.db.ComponentCacheInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.concurrent.atomic.AtomicInteger;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class WorkJob {

    ComponentCacheInfo cacheInfo;

    String tableNames;

    String currentTableName;

    String currentTableNameValue;

    AtomicInteger currentTableCount = new AtomicInteger(0);





}
